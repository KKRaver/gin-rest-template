package models

type (
	User struct {
		ID       uint   `gorm:"primary_key" json:"id"`
		Username string `json:"username" gorm:"unique" binding:"required,min=6"`
		Password string `json:"password,omitempty" binding:"required,min=8"`
		Email    string `json:"email" gorm:"unique" binding:"required,email"`
	}
	UserResponse struct {
		Data User `json:"user"`
	}
	UsersResponse struct {
		Data []User `json:"users"`
	}
	UserUri struct {
		ID string `uri:"id" binding:"required,min=1,number"`
	}
)
