package models

import "github.com/dgrijalva/jwt-go"

type (
	Token struct {
		UserID uint
		Name string
		Email string
		*jwt.StandardClaims
	}
	TokenResponse struct {
		Token string `json:"token"`
		UserResponse
	}
	LoginRequest struct {
		Username string `json:"username" binding:"required"`
		Password string `json:"password" binding:"required"`
	}
)
