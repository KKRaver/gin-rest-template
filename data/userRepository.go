package data

import (
	"golang-gin-web-framework/models"
	"gorm.io/gorm"
	"golang.org/x/crypto/bcrypt"
	"log"
)

type UserRepository struct {
	DB *gorm.DB
}

func (r *UserRepository) Create(user *models.User) error {
	pass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.Password = string(pass)
	db := r.DB.Create(user)
	return db.Error
}

func (r *UserRepository) CompareAuth(username, password string) (*models.User, error) {
	user := &models.User{}

	if err := r.DB.Where("Username = ?", username).First(&user).Error; err != nil {
		return nil, err
	}
	log.Printf("%v", username)
	log.Printf("%v", user.Username)
	log.Printf("%v", user.ID)
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return nil, err
	}
	return user, nil
}

func (r *UserRepository) FindOne(id uint) (*models.User, error) {
	user := &models.User{}
	user.ID = id
	if err := r.DB.First(&user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

func (r *UserRepository) GetAll() ([]models.User, error) {
	var users []models.User
	if err := r.DB.Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}

func (r *UserRepository) Delete(id uint) error {
	user := &models.User{}
	user.ID = id
	if err := r.DB.Delete(user).Error; err != nil {
		return err
	}
	return nil
}