package common

import (
	"encoding/json"
	"log"
	"os"
)

type configuration struct {
	Server, PostgresqlHost, PostgresqlPort, DBUser, DBPassword, Database, Secret string
}

var AppConfig configuration

func initConfig() {
	loadAppConfig()
}

func loadAppConfig() {
	file, err := os.Open("config.json")
	if err != nil {
		log.Fatalf("[loadConfig]: %s\n", err)
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&AppConfig)
	if err != nil {
		log.Fatalf("[loadAppConfig]: %s\n", err)
	}
}