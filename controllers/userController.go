package controllers

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang-gin-web-framework/common"
	"golang-gin-web-framework/data"
	"golang-gin-web-framework/models"
	"gorm.io/gorm"
	"net/http"
	"strconv"
	"time"
)

func Register(c *gin.Context) {
	var user models.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Provided JSON was invalid", "error": err.Error()})
		return
	}
	db := common.GetSession()
	repo := &data.UserRepository{DB: db}
	if err := repo.Create(&user); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Could not save to database", "error": err.Error()})
		return
	}
	user.Password = ""
	response := models.UserResponse{Data: user}
	c.JSON(http.StatusOK, response)
}

func LoginUser(c *gin.Context) {
	var loginRequestData models.LoginRequest
	if err := c.ShouldBindJSON(&loginRequestData); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Provided JSON was invalid", "error": err.Error()})
		return
	}
	db := common.GetSession()
	repo := data.UserRepository{DB: db}
	loggedUser, err := repo.CompareAuth(loginRequestData.Username, loginRequestData.Password)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "Wrong login or password", "error": err.Error()})
		return
	}
	expiresAt := time.Now().Add(time.Minute * 1000).Unix()
	tk := &models.Token{
		UserID: loggedUser.ID,
		Name:   loggedUser.Username,
		Email:  loggedUser.Email,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)

	tokenString, err := token.SignedString([]byte(common.AppConfig.Secret))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Could not sign the token", "error": err.Error()})
		return
	}
	loggedUser.Password = ""
	c.JSON(http.StatusOK, models.TokenResponse{Token: tokenString, UserResponse: models.UserResponse{Data: *loggedUser}})
}

func GetAllUsers(c *gin.Context) {
	db := common.GetSession()
	repo := &data.UserRepository{DB: db}
	users, err := repo.GetAll()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Could not fetch from database", "error": err.Error()})
		return
	}
	for index, user := range users {
		user = users[index]
		user.Password = ""
		users[index] = user
	}
	usersResponse := models.UsersResponse{Data: users}
	c.JSON(http.StatusOK, usersResponse)
}

func GetSingleUser(c *gin.Context) {
	var uri models.UserUri
	if err := c.ShouldBindUri(&uri); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Bad user id, %v", "error": err.Error()})
		return
	}
	db := common.GetSession()
	repo := data.UserRepository{DB: db}
	id, err := strconv.Atoi(uri.ID)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Bad user id", "error": err.Error()})
		return
	}
	user, err := repo.FindOne(uint(id))

	if err != nil {
		if err == gorm.ErrRecordNotFound {
			c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"message": "User not found", "error": err.Error()})
			return
		}
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Could not fetch from database", "error": err.Error()})
		return
	}

	user.Password = ""
	userResponse := models.UserResponse{Data: *user}
	c.JSON(http.StatusOK, userResponse)
}

func GetCurrentUser(c *gin.Context) {
	tk, _ := c.Get("token")
	token := tk.(*models.Token)
	db := common.GetSession()
	repo := data.UserRepository{DB: db}
	user, err := repo.FindOne(token.UserID)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "Could not fetch from database", "error": err.Error()})
		return
	}
	user.Password = ""
	userResponse := models.UserResponse{Data: *user}
	c.JSON(http.StatusOK, userResponse)
}

func DeleteUser(c *gin.Context) {
	var uri models.UserUri
	if err := c.ShouldBindUri(&uri); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Bad user id", "error": err.Error()})
		return
	}
	db := common.GetSession()
	repo := data.UserRepository{DB: db}
	id, err := strconv.Atoi(uri.ID)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Bad user id", "error": err.Error()})
		return
	}
	err = repo.Delete(uint(id))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "Cannot delete user", "error": err.Error()})
		return
	}
	c.Status(http.StatusNoContent)
}