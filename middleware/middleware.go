package middleware

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang-gin-web-framework/models"
	"net/http"
	"strings"
)

// JwtVerify Middleware function
func JwtVerify() gin.HandlerFunc {
	return func(c *gin.Context) {

		var header = c.GetHeader("Authorization")

		header = strings.TrimSpace(header)

		if header == "" {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "Authorization header is missing or empty"})
			return
		}
		tk := &models.Token{}

		_, err := jwt.ParseWithClaims(header, tk, func(token *jwt.Token) (interface{}, error) {
			return []byte("secret"), nil
		})

		if err != nil {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{"message": "Token is invalid"})
			return
		}
		c.Set("logged_in", true)
		c.Set("token", tk)
	}
}