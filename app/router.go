package app

import (
	"golang-gin-web-framework/controllers"
	"golang-gin-web-framework/middleware"
)

func InitRoutes() {

	router.POST("/register", controllers.Register)
	router.POST("/login", controllers.LoginUser)
	router.GET("/current_user", middleware.JwtVerify(), controllers.GetCurrentUser)


	router.GET("/users", controllers.GetAllUsers)
	router.GET("/users/:id", middleware.JwtVerify(), controllers.GetSingleUser)
	router.DELETE("/users/:id", middleware.JwtVerify(), controllers.DeleteUser)
}
