package app

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"golang-gin-web-framework/common"
	"log"
	"os"
)

var router *gin.Engine

func StartApp() {
	common.StartUp()
	router = gin.Default()
	InitRoutes()
	if err := router.Run(getPort()); err != nil {
		log.Fatalf("Failed to start server")
	}
}

func getPort() string {
	var port = os.Getenv("PORT")
	if port == "" {
		port = common.AppConfig.Server
		fmt.Println("INFO: No PORT env var detected, defaulting to "+ port)
	}
	return ":" + port
}